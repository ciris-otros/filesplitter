package com.ciriscr.filesplitter

import scala.collection.mutable.Map

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/13
 * Time: 11:17 AM
 */

trait Writter {
  val filePath: String

  protected val log = new Logger(filePath)
  def writeLine(line: String)
  def close() = log.cerrar
}

case class LineWritter(filePath: String) extends Writter {
  override def writeLine(line: String): Unit = {
    log.escribir(line)
  }
}
