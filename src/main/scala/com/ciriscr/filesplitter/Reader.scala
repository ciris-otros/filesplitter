package com.ciriscr.filesplitter

import scala.io.Source

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        09/10/14
 * Hora:         11:45 AM
 */
case class Reader(isFirstLineHeader: Boolean, fileSize: Int, inputFileName: String, outputFileName: Option[String],
                  outputWithHeader: Boolean) {
  import scala.collection.mutable.Map

  val map = Map.empty[Int, Writter]
  val (outName, outExtention) = {
    val arr = outputFileName.getOrElse(inputFileName).split('.')
    if (arr.size > 1) arr.init.mkString(".") -> arr.last
    else arr.mkString(".") -> ""
  }
  var header: String = ""

  def read(): Unit = {
    val it = Source.fromFile(inputFileName).getLines()
    if (isFirstLineHeader) header = it.next()
    var i = 0
    while (it.hasNext) {
      getOutputFile(i / fileSize).writeLine(it.next())    // i / filesize return an Int because both are Int's
      i += 1
    }
    closeWritters()
  }

  private def getOutputFile(key: Int): Writter = map.getOrElseUpdate(key, newWritter(key))

  private def closeWritters(): Unit = map.map(w => w._2.close())

  private def newWritter(key: Int) = {
    val writter = LineWritter(s"${outName}_$key.$outExtention")
    if (outputWithHeader) writter.writeLine(header)
    writter
  }
}
